﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EverbridgeApi.Models
{
    public class Address
    {
        public string streetAddress { get; set; }
        public string postalCode { get; set; }
        public string source { get; set; }
        public string state { get; set; }
        public string locationName { get; set; }
        public string suite { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public int locationId { get; set; }
        public int locationSourceId { get; set; }
    }
}