﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EverbridgeApi.Models
{
    public class Group
    {
        public string createdName { get; set; }
        public long lastModifiedTime { get; set; }
        public int accountId { get; set; }
        public string status { get; set; }
        public int resourceBundleId { get; set; }
        public long organizationId { get; set; }
        public long id { get; set; }
        public int parentId { get; set; }
        public string name { get; set; }
        public long lastModifiedDate { get; set; }
        public long lastModifiedId { get; set; }
        public long createdId { get; set; }
        public long createdDate { get; set; }
        public long lastSynchronizedTime { get; set; }
        public string lastModifiedName { get; set; }
        public bool dirty { get; set; }
    }
}