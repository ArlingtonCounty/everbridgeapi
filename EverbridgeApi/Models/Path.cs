﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EverbridgeApi.Models
{
    public class Path
    {
        public int waitTime { get; set; }
        public object pathId { get; set; }
        public string countryCode { get; set; }
        public string value { get; set; }
        public bool skipValidation { get; set; }
    }
}