﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EverbridgeApi.Models
{
    public class Contact
    {
        public long lastModifiedTime { get; set; }
        public long organizationId { get; set; }
        public string registedEmail { get; set; }
        public string registeredEmail { get; set; }
        public long createdDate { get; set; }
        public List<long> groups { get; set; }
        public string createdName { get; set; }
        public string lastName { get; set; }
        public string status { get; set; }
        public long registedDate { get; set; }
        public long registeredDate { get; set; }
        public string country { get; set; }
        public long recordTypeId { get; set; }
        public string lastModifiedName { get; set; }
        public int accountId { get; set; }
        public long individualAccountId { get; set; }
        public string externalId { get; set; }
        public long id { get; set; }
        public List<Path> paths { get; set; }
        public string firstName { get; set; }
        public bool uploadProcessing { get; set; }
        public int resourceBundleId { get; set; }
        public List<object> topics { get; set; }
        public List<Address> address { get; set; }
        public long createdId { get; set; }
        public long lastModifiedId { get; set; }
        public long lastModifiedDate { get; set; }
    }
}