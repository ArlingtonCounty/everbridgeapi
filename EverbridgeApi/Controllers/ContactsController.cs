﻿using EverbridgeApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace EverbridgeApi.Controllers
{
    public class ContactsController : ApiController
    {
        long organizationId = Convert.ToInt64(ConfigurationManager.AppSettings["organizationId"]);
        string authentication = ConfigurationManager.AppSettings["authentication"].ToString();
        long recordTypeId = Convert.ToInt64(ConfigurationManager.AppSettings["recordTypeId"]);

        public IEnumerable<string> Get()
        {
            throw new NotImplementedException();
        }

        public Contact Get(string id, string idType)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            WebRequest request = WebRequest.Create($"https://api.everbridge.net/rest/contacts/{organizationId}/{id}?idType={idType}");
            request.Method = "GET";
            request.Headers.Add("Authorization", authentication);

            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode.ToString() == "OK")
                {
                    var responseStream = response.GetResponseStream();
                    StreamReader _answer = new StreamReader(responseStream);
                    var result = _answer.ReadToEnd();
                    var responseData = JsonConvert.DeserializeObject<dynamic>(result);

                    if (responseData.result == null || String.IsNullOrEmpty(responseData.result.ToString()))
                        return null;
                    else
                    {
                        var contactData = JsonConvert.DeserializeObject<Contact>(responseData.result.ToString());
                        return contactData;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public void Post(Contact contact)
        {
            //Add/Update recordtypeId from configs
            contact.recordTypeId = recordTypeId;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            WebRequest webRequest = WebRequest.Create($"https://api.everbridge.net/rest/contacts/{organizationId}");
            webRequest.Method = "POST";
            webRequest.Headers.Add("Authorization", authentication);
            webRequest.ContentType = "application/json";

            Stream dataStream = webRequest.GetRequestStream();

            //Convert to json
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            var json = JsonConvert.SerializeObject(contact, settings);
            var bytes = Encoding.UTF8.GetBytes(json);

            //Write to request stream
            var stream = webRequest.GetRequestStream();
            stream.Write(bytes, 0, bytes.Length);

            dataStream.Close();
            try
            {
                var response = webRequest.GetResponse();
                Console.WriteLine($"Created: {contact.externalId}");
                response.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Put(int id, [FromBody]string value)
        {
            throw new NotImplementedException();
        }

        public void Delete(string id, string idType)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            WebRequest webRequest = WebRequest.Create($"https://api.everbridge.net/rest/contacts/{organizationId}/{id}?idType={idType}");
            webRequest.Method = "DELETE";
            webRequest.Headers.Add("Authorization", authentication);
            webRequest.ContentType = "application/json";

            try
            {
                var response = webRequest.GetResponse();
                Console.WriteLine($"Deleted: {id}");
                response.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
