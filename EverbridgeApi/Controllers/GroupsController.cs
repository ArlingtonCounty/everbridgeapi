﻿using EverbridgeApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace EverbridgeApi.Controllers
{
    public class GroupsController : ApiController
    {
        long organizationId = Convert.ToInt64(ConfigurationManager.AppSettings["organizationId"]);
        string authentication = ConfigurationManager.AppSettings["authentication"].ToString();

        public IEnumerable<string> Get()
        {
            throw new NotImplementedException();
        }

        public Group Get(string id, string idType)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            WebRequest request = WebRequest.Create($"https://api.everbridge.net/rest/groups/{organizationId}/{id}?queryType={idType}");
            request.Method = "GET";
            request.Headers.Add("Authorization", authentication);

            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode.ToString() == "OK")
                {
                    var responseStream = response.GetResponseStream();
                    StreamReader _answer = new StreamReader(responseStream);
                    var result = _answer.ReadToEnd();
                    var responseData = JsonConvert.DeserializeObject<dynamic>(result);

                    if (responseData.result == null || String.IsNullOrEmpty(responseData.result.ToString()))
                        return null;
                    else
                    {
                        var groupData = JsonConvert.DeserializeObject<Group>(responseData.result.ToString());
                        return groupData;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public void Post([FromBody]string value)
        {
            throw new NotImplementedException();
        }

        public void Put(int id, [FromBody]string value)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
