﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EverbridgeApi.Controllers;
using EverbridgeApi.Models;
using System.IO;
using Newtonsoft.Json;
using System.Net;

namespace EverbridgeApi.Tests
{
    /// <summary>
    /// Tests Contacts Controllers Action Methods
    /// </summary>
    [TestClass]
    public class ContactTests
    {
        [TestMethod]
        public void GetContact_ShouldReturnCorrectContact()
        {
            using (StreamReader stream = new StreamReader(@"C:\GIT\everbridgeapi\EverbridgeApi.Tests\testcontact.json"))
            {
                // Arrange
                var contact = JsonConvert.DeserializeObject<Contact>(stream.ReadToEnd());
                var controller = new ContactsController();

                // Act
                var actual = controller.Get(contact.externalId, "externalId");

                // Assert
                Assert.IsNotNull(actual);
                Assert.AreEqual(contact.externalId, actual.externalId);
            }
        }

        [TestMethod]
        public void AddContact_ShouldCreateNewContact()
        {
            using (StreamReader stream = new StreamReader(@"C:\GIT\everbridgeapi\EverbridgeApi.Tests\testcontact.json"))
            {
                // Arrange
                var contact = JsonConvert.DeserializeObject<Contact>(stream.ReadToEnd());
                var controller = new ContactsController();

                // Act
                controller.Post(contact);
                var actual = controller.Get(contact.externalId, "externalId");

                // Assert
                Assert.IsNotNull(actual);
                Assert.AreEqual(contact.externalId, actual.externalId);
            }
        }

        [TestMethod]
        public void DeleteContact_ShouldDeleteContact()
        {
            using (StreamReader stream = new StreamReader(@"C:\GIT\everbridgeapi\EverbridgeApi.Tests\testcontact.json"))
            {
                // Arrange
                var contact = JsonConvert.DeserializeObject<Contact>(stream.ReadToEnd());
                var controller = new ContactsController();

                // Act
                controller.Delete(contact.externalId, "externalId");
                Contact actual = null;
                try
                {
                    actual = controller.Get(contact.externalId, "externalId");
                }
                catch (Exception ex)
                {
                    if (!ex.Message.Contains("404"))
                        throw ex;
                }

                // Assert
                Assert.IsNull(actual);
            }
        }
    }
}
