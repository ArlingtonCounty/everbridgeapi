﻿using EverbridgeApi.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverbridgeApi.Tests
{
    /// <summary>
    /// Tests Groups Controllers Action Methods
    /// </summary>
    [TestClass]
    public class GroupTests
    {
        [TestMethod]
        public void GetGroupTests_ShouldReturnCorrectGroup()
        {
            // Arrange
            var controller = new GroupsController();
            var expectedGroupName = "DTS";

            // Act
            var actual = controller.Get("DTS", "name");

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expectedGroupName, actual.name);
        }
    }
}
